\chapter{Hijerarhijska struktura ome�uju�ih volumena}

Mogu�e je ubrzati izvr�avanje algoritma pra�enja zrake svjetlosti tako da se objekti u sceni \navod{omotaju} odgovaraju�im ome�uju�im volumenima i grupiraju u hijerarhijsku strukturu ome�uju�ih volumena (eng. \gls{bvh}) koja spada u podatkovnu strukturu stabla. Rad sa stablima mo�e imati logaritmi�nu slo�enost, za razliku od rada s nizovima objekata koji imaju linearnu slo�enost, �ime se mogu dobiti bolje performanse \cite{35_2017}. 

Na slici \ref{fig:bvh1} \cite{35_2017} prikazan je primjer hijerarhije ome�uju�ih volumena petero jednostavnih objekata. Tip ome�uju�eg volumena je kvadar poravnat s osima kartezijevog koordinatnog sustava (eng. \gls{aabb}) a vi�e informacija o njemu nalazi se u potpoglavlju \ref{sec:AABB}.

\begin{figure}[!htbp]
    \begin{center}
        \includegraphics{bvh}
        \caption{Primjer hijerarhije AABB ome�uju�ih volumena}
        \label{fig:bvh1}
    \end{center}
\end{figure}

Ovakve se strukture mogu koristiti u mnogo slu�ajeva u ra�unalnoj grafici kada treba primjenjivati odre�ene testove na objektima. U ovom radu osnovni test je ispitivanje mogu�eg presjeka zrake i objekta. Do sada, algoritam pra�enja zrake svjetlosti je funkcionirao tako da je ispitivao presjek zrake sa svim objektima u sceni. S ovakvom hijerarhijom, kada se ispituje presjek zrake s objektima, potrebno je krenuti od vrha stabla i testirati sije�e li zraka ome�uju�i volumen koji pripada tom vrhu. Ukoliko ne sije�e, sigurno ne�e sje�i bilo koji objekt sadr�an unutar tog volumena �to zna�i da te objekte ne trebamo ispitivati �ime se posti�e �tednja resursa i pobolj�avaju performanse. Ukoliko zraka sije�e taj volumen, trebamo ispitati objekte koji pripadaju tom volumenu. 

Jedan mogu�i scenarij bio bi, prema slici \ref{fig:bvh1}, da zraka sije�e objekt A. Prvo bismo testirali presjek za ome�uju�i volumen svih objekata stabla. Ustvrdili bismo da presjek postoji, te bi zatim testirali presjek za dvoje djece vrha stabla. Presjek za ome�uju�i volumen objekata B i E ne postoji, �to zna�i da ih odbacujemo. Presjek za ome�uju�i volumen objekata C, D i A postoji, pa prelazimo na to podstablo. Ovdje detektiramo presjek sa A i proma�aj za ome�uju�i volumen objekata C i D.

Konstrukcija \gls{bvh}-a je slo�en i netrivijalan zadatak, kojeg je mogu�e odraditi na mnogo na�ina. Teoretski, taj se posao mo�e dodijeliti umjetnicima i dizajnerima, no na�in organiziranja komponenti i primitiva slo�enih modela koji najvi�e odgovara njihovom poslu (kod organizacije po funkcionalnosti, primjerice, sve komponente istog tipa, koliko god bile me�usobno udaljene u prostoru slo�enog objekta, mogu biti grupirane u isti �vor hijerarhije) ne odgovara zahtjevima koji postoje kod implementacije \gls{bvh}-a (grupiranje po prostornoj blizini). Zato je potrebno koristiti metode automatizirane izgradnje hijerarhije �to je opisano u potpoglavlju \ref{sec:BVH_Imp}. 

\section{Presjek zrake i AABB-a} \label{sec:AABB}

Izvr�avanje geometrijskih testova sa slo�enim objektima mo�e biti ra�unski skupa operacija. Zbog toga se koriste ome�uju�i volumeni (eng. \emph{bounding volumes}), jednostavni volumeni (npr. kvadri, sfere i sl.) koji unutar sebe sadr�avaju slo�eni objekt i kod kojih su testovi mnogo ra�unski jeftiniji.

Naravno, ukoliko ti testovi imaju pozitivan ishod, svejedno je potrebno testirati te slo�ene objekte, no gledaju�i op�enito, posti�u se veliki pomaci u performansama jer za mnogo slu�ajeva ti testovi ne�e prolaziti a slo�eni objekti u tim slu�ajevima se ne�e morati ispitivati.

U ovom radu sam odabrao \gls{aabb}, zbog dobre pokrivenosti u literaturi, jednostavnosti implementacije te postojanja robusnog i efikasnog algoritma za ra�unanje presjeka zrake s njim \cite{33_2017} koji uzima u obzir razna svojstva IEEE standarda za \emph{floating-point} format brojeva. Razumijevanje logike algoritma sam prona�ao na \cite{34_2017}.

Algoritam tra�i da se \gls{aabb} definira uz pomo� dvije to�ke: \emph{min} i \emph{max}, koje predstavljaju najmanju i najve�u to�ku kvadra, odnosno njegova dva nasuprotna kuta. Ovdje, za razliku od prija�njih izvoda za presjek zrake i objekta, dovoljno je samo znati da li zraka sije�e kvadar ili ne sije�e. Ostali podaci, poput udaljenosti i normale povr�ine nam nisu va�ni. U po�etku, izgradnju algoritma je dobro zapo�eti sa \acrshort{2d} AABB-om zbog jednostavnosti, �to se kasnije mo�e generalizirati na \acrshort{3d}.

\acrshort{2d} \gls{aabb} (pravokutnik) je definiran sa dvije horizontalne i dvije vertikalne linije, kako je prikazano u jednad�bama \ref{subeq:10.1}.

\begin{subequations}
    \begin{align}
    x &= x_{min} \\
    x &= x_{max} \\
    y &= y_{min} \\
    y &= y_{max}
    \end{align}
    \label{subeq:10.1}
\end{subequations}

To�ke tog pravokutnika pripadaju intervalu definiranom s tim linijama, po izrazu \eqref{eq:10.2}.

\begin{equation}
(x, y)\, \epsilon \, [x_{min}, x_{max}] \times [y_{min}, y_{max}] \label{eq:10.2}
\end{equation}

Zraka se definira izrazom \eqref{subeq:10.3.1}.

\begin{subequations}
    \begin{align}
    R(t) &= R_{0} + R_{d}t,\,t > 0 \label{subeq:10.3.1} \\
    R_{0} &= [x_{0}, y_{0}, z_{0}] \label{subeq:10.3.2} \\
    R_{d} &= [x_{d}, y_{d}, z_{d}] \label{subeq:10.3.3}
    \end{align}
    \label{subeq:10.3}
\end{subequations}

Kako bismo dobili izraz za izra�un parametra $ t $ u kojem zraka sije�e liniju $ x = x_{min} $, izjedna�imo $ x $ komponentu izraza \eqref{subeq:10.3.1} s tom linijom, kako je prikazano u jednad�bi \eqref{eq:10.4}. Taj parametar �e se zvati $ t_{xmin} $.

\begin{equation}
x_{0} + x_{d}t_{xmin} = x_{min} \label{eq:10.4}
\end{equation}

Nakon sre�ivanja imamo tra�eni izraz \eqref{eq:10.5}.

\begin{equation}
t_{xmin} = \frac{x_{min} - x_{0}}{x_{d}} \label{eq:10.5}
\end{equation}

Proces je analogan za preostale tri linije. Zraka sije�e \gls{aabb} ako i samo ako se intervali $ [t_{xmin}, t_{xmax}]  $ i $ [t_{ymin}, t_{ymax}] $ preklapaju, odnosno njihov presjek nije prazan. Osnovni algoritam bi prema tim pretpostavkama bio onaj u pseudokodu \ref{alg:10.1}.

\begin{algorithm}
    \caption{Osnovni algoritam za presjek 2D zrake i AABB-a}
    \label{alg:10.1}
    \begin{algorithmic}[1]
    \State $ t_{xmin} = (x_{min} - x_{0}) / x_{d} $
    \State $ t_{xmax} = (x_{max} - x_{0}) / x_{d} $
    \State $ t_{ymin} = (y_{min} - y_{0}) / y_{d} $
    \State $ t_{ymax} = (y_{max} - y_{0}) / y_{d} $
    \If {$ t_{xmin} > t_{ymax} $ or $ t_{ymin} > t_{xmax} $} \Return false \label{alg:10.1.1}
    \Else $ \, $ \Return  true
    \EndIf
    \end{algorithmic}
\end{algorithm}

Uvjet koji postoji u liniji \ref{alg:10.1.1} podrazumijeva da presjeka izme�u dva intervala nema ako je prvi interval u potpunosti lijevo ili desno od drugog intervala. Treba uzeti u obzir slu�aj kada su $ x_{d} $ ili $ y_{d} $ negativni. Primjerice, ukoliko vrijedi $ x_{d} < 0 $, zraka �e pogoditi $ x_{max} $ prije nego �to pogodi $ x_{min} $. Pobolj�ani pseudokod \ref{alg:10.2} prikazuje primjer za ra�unanje $  t_{xmin} $ i $  t_{xmax} $ a postupak je analogan kod $ t_{ymin} $ i $ t_{ymax} $.

\begin{algorithm}
    \caption{Pseudokod za pobolj�ani izra�un $  t_{xmin} $ i $  t_{xmax} $}
    \label{alg:10.2}
    \begin{algorithmic}[1]
        \If {$ x_{d} \geq 0 $}
            \State $ t_{xmin} = (x_{min} - x_{0}) / x_{d} $
            \State $ t_{xmax} = (x_{max} - x_{0}) / x_{d} $
        \Else
            \State $ t_{xmin} = (x_{max} - x_{0}) / x_{d} $
            \State $ t_{xmax} = (x_{min} - x_{0}) / x_{d} $   
        \EndIf
    \end{algorithmic}
\end{algorithm}

Znamo da horizontalne zrake imaju $ y_{d} = 0 $ a vertikalne zrake $ x_{d} = 0 $. To �e uzrokovati dijeljenje s nulom, �to bi mogao biti problem. Me�utim, IEEE standard se dobro nosi s tim slu�ajevima. Kod njega vrijede zakonitosti opisane u izrazima \eqref{subeq:10.6}, gdje je $ a $ bilo koji pozitivni realni broj.

\begin{subequations}
    \begin{align}
    +a/0 &= +\infty \\
    -a/0 &= -\infty
    \end{align}
    \label{subeq:10.6}
\end{subequations}

Uzmimo za primjer slu�aj gdje imamo $ x_{d} = 0 $ i $ y_{d} > 0 $. Mo�emo obaviti izra�une prema izrazima \eqref{subeq:10.7}.

\begin{subequations}
    \begin{align}
    t_{xmin} &= \frac{x_{min} - x_{0}}{0} \\
    t_{xmax} &= \frac{x_{max} - x_{0}}{0} 
    \end{align}
    \label{subeq:10.7}
\end{subequations}

Postoje tri mogu�nosti:

\begin{enumerate}
    \item $ x_{0} \leq x_{min} $ (nema presjeka)
    \item $ x_{min} < x_{0} < x_{max} $ (presjek postoji)
    \item $ x_{max} \leq x_{0} $ (nema presjeka)
\end{enumerate}

U prvom slu�aju imamo situaciju opisanu u izrazima \eqref{subeq:10.8}.

\begin{subequations}
    \begin{align}
    t_{xmin} &= \frac{pozitivni \, broj}{0} \\
    t_{xmax} &= \frac{pozitivni \, broj}{0} 
    \end{align}
    \label{subeq:10.8}
\end{subequations}

Time dobivamo interval $ (t_{xmin}, t_{xmax}) = (\infty, \infty) $ koji se ne preklapa ni sa kojim intervalom, �to nam odgovara, jer �elimo da tada nema presjeka zrake i  \gls{aabb}-a. Drugi slu�aj, opisan u izrazima \eqref{subeq:10.9}, nam daje interval $ (t_{xmin}, t_{xmax}) = (-\infty, \infty) $, koji se preklapa s bilo kojim intervalom, �to nam tako�er odgovara.

\begin{subequations}
    \begin{align}
    t_{xmin} &= \frac{negativni \, broj}{0} \\
    t_{xmax} &= \frac{pozitivni \, broj}{0} 
    \end{align}
    \label{subeq:10.9}
\end{subequations}

Tre�i slu�aj daje interval $ (-\infty, -\infty) $ koji isto rezultira nepostojanjem presjeka. Mo�emo zaklju�iti kako konvencije u IEEE formatu ovdje idu nama u prilog te ne moramo raditi posebne provjere za ove slu�ajeve. Me�utim, postoji jedan problem koji se javlja kada je $ x_{d} = -0,0 $ (u pseudokodu \ref{alg:10.2}). U tom slu�aju, uvjet $ x_{d} \geq 0 $ je prema pravilima IEEE formata istinit, jer u njemu stoji da je $ -0 == 0 $ istinita tvrdnja. Interval koji tada dobijemo je $ (t_{xmin}, t_{xmax}) = (\infty, -\infty) $ a algoritam �e za taj interval vratiti $ false $, �to nije ispravno jer nije detektiran presjek zrake i \gls{aabb}-a koji postoji. Taj se problem rje�ava kako je prikazano u pseudokodu \ref{alg:10.3}.

\begin{algorithm}
    \caption{Rje�enje problema koji nastaje kada je $ x_{d} = -0,0 $}
    \label{alg:10.3}
    \begin{algorithmic}[1]
        \State $ a = 1/x_{d} $
        \If {$ a \geq 0 $}
        \State $ t_{xmin} = a(x_{min} - x_{0}) $
        \State $ t_{xmax} = a(x_{max} - x_{0}) $
        \Else
        \State $ t_{xmin} = a(x_{max} - x_{0}) $
        \State $ t_{xmax} = a(x_{min} - x_{0}) $   
        \EndIf
    \end{algorithmic}
\end{algorithm}

Na ovaj na�in, dobivamo ispravni interval jer vrijedi $ \frac{1}{0} = +\infty $ i $ \frac{1}{-0} = -\infty $. Usput, �esto je slu�aj da su dvije operacije mno�enja br�e od jednog dijeljenja kojeg zamjenjuju.

Budu�i da je vrijednost $ a $, odnosno $ inv\_dir $ potrebno izra�unati samo jednom a koristi se svaki put kod testiranja presjeka zrake i AABB-a, mo�emo ju zapisati u sam objekt \emph{Ray}, kao i neke dodatne podatke, �to je prikazano u isje�ku koda \ref{code:10.1} u metodi \emph{calc\_cache}.

\begin{figure}[!htbp]
    \begin{lstlisting}[style=myC++,label={code:10.1}, caption={Klasa Ray}]
class Ray
{
    public:
        Ray() : start(Vector3f()), dir(Vector3f()){calc_cache();}
        Ray(const Vector3f &st, const Vector3f &d) : start(st), dir(d){calc_cache();}
        Ray(const Vector3f &st, const Vector3f &d, const bool is_light) : start(st), dir(d), isShadowRay(is_shadow){calc_cache();}
        Vector3f start, dir, inv_dir;
        int sign[3];
        bool isShadowRay = false;
    private:
        void calc_cache()
        {
            inv_dir = 1.0f / dir;
            sign[0] = (inv_dir.x < 0);
            sign[1] = (inv_dir.y < 0);
            sign[2] = (inv_dir.z < 0);
        }
};
    \end{lstlisting}
\end{figure}

Ti se podaci koriste u optimiziranoj verziji algoritma za presjek zrake i \gls{aabb}-a \cite{33_2017}, kojeg koristim u svom radu, a prikazan je u isje�ku koda \ref{code:10.2}.

\begin{figure}[!htbp]
    \begin{lstlisting}[style=myC++,label={code:10.2}, caption={Metoda intersect u klasi AABB}]
bool AABB::intersect(const Ray &ray, intersectionList& list) const
{
    if(right == NULL && left)
        return left->intersect(ray, list);

    glm::float32 t_min, t_max, t_min_y, t_max_y, t_min_z, t_max_z;

    t_min = (bounds[ray.sign[0]].x - ray.start.x) * ray.inv_dir.x;
    t_max = (bounds[1 - ray.sign[0]].x - ray.start.x) * ray.inv_dir.x;

    t_min_y = (bounds[ray.sign[1]].y - ray.start.y) * ray.inv_dir.y;
    t_max_y = (bounds[1 - ray.sign[1]].y - ray.start.y) * ray.inv_dir.y;

    if( (t_min > t_max_y) || (t_min_y > t_max) )
        return false;

    if(t_min_y > t_min)
        t_min = t_min_y;

    if(t_max_y < t_max)
        t_max = t_max_y;

    t_min_z = (bounds[ray.sign[2]].z - ray.start.z) * ray.inv_dir.z;
    t_max_z = (bounds[1 - ray.sign[2]].z - ray.start.z) * ray.inv_dir.z;

    if( (t_min > t_max_z) || (t_min_z > t_max) )
        return false;

    bool child_intersect_1 = false;
    bool child_intersect_2 = false;

    if(left)
        child_intersect_1 = left->intersect(ray, list);

    if(right)
        child_intersect_2 = right->intersect(ray, list);

    if(child_intersect_1 || child_intersect_2)
        return true;
    else
        return false;
}
    \end{lstlisting}
\end{figure}

Generalizacija na tri osi, odnosno uklju�ivanje $ z $ osi u prora�une se vr�i tako da se gleda presjek sva tri intervala umjesto samo dva kao za dvodimenzionalni slu�aj.

Va�no je naglasiti kako je ovdje implementirana i funkcionalnost putovanja po \gls{aabb} stablu, opisanog u potpoglavlju \ref{sec:BVH_Imp}. Svaki objekt tipa \gls{aabb} ima \emph{left} i \emph{right}, odnosno lijevo i desno dijete, koje mo�e biti jo� jedan \emph{SolidObject}. Ako postoje ta djeca, rekurzivno se ispituje mogu�i presjek s njima. Poseban slu�aj se nalazi na samom po�etku isje�ka \ref{code:10.2}, gdje ukoliko AABB ima samo jedno dijete, preska�emo izra�une vezane za presjek zrake i \gls{aabb}-a jer su nepotrebni.

Najva�nija prednost ovakve implementacije putovanja po \gls{aabb} stablu je �injenica da nije bilo potrebno mijenjati algoritam pra�enja zrake svjetlosti. On, kao i ina�e, ispituje presjek sa svim objektima u sceni te dobije potrebne podatke od objekta tipa \gls{aabb}.

\section{Implementacija BVH-a} \label{sec:BVH_Imp}

Osnovni zadatak izgradnje \gls{bvh}-a je raspodijeliti skup ulaznih objekata u neku strukturu stabla. Broj mogu�ih stabala koja se mogu izgraditi od nekog skupa objekata eksponencijalno raste s koli�inom tih objekata. Zbog jednostavnosti implementacije i popularnosti, odabrao sam pristup \navod{odozgo prema dolje} (eng. \emph{top-down approach}), koji funkcionira tako da se za ulazni skup objekata odredi ome�uju�i volumen, izvr�i podjela na dva podskupa te se za svaki od njih rekurzivno odra�uje isto: odredi se ome�uju�i volumen te se izvr�i podjela na dva podskupa, do odre�enog \emph{stop} kriterija, kojih mo�e biti i vi�e. Svaki podskup je povezan sa skupom od kojeg je nastao. Podskup je u tom slu�aju dijete, a skup roditelj, te se formira hijerarhija. Budu�i da za tip ome�uju�eg volumena koristim \gls{aabb}, vrsta \gls{bvh}-a u mom radu je \gls{aabb} stablo, inspirirano njegovim opisom u knjizi \cite{35_2017}, s malim preinakama koje sam poduzeo kako bih dobio �to jednostavniju kona�nu implementaciju. U isje�ku koda \ref{code:10.3} prikazana je moja implementacija izgradnje \gls{aabb} stabla.

\begin{figure}[!htbp]
    \begin{lstlisting}[style=myC++,label={code:10.3}, caption={Konstruktor za izgradnju AABB stabla}]
if(objects.size() == 0)
   return;
else if(objects.size() == 1)
{
   left = objects[0]; return;
}
else if(objects.size() == 2)
{
   ComputeBoundingBox(objects);
   left = objects[0]; right = objects[1];
   return;
}
ComputeBoundingBox(objects);
Axis longest_axis = x;
glm::float32 splitting_point = 0.0f;
bool first_pass = true;
splitting_point = ComputeSpatialMedian(longest_axis);
SolidObjects subset_1, subset_2;
create_subsets:
for(unsigned int i = 0; i < objects.size(); ++i)
{
   if(longest_axis == x)
   {
       if(objects[i]->centroid.x < splitting_point) { subset_1.push_back(objects[i]); }
       else { subset_2.push_back(objects[i]); }
   }
   else if(longest_axis == y)
   {
       if(objects[i]->centroid.y < splitting_point) { subset_1.push_back(objects[i]); }
       else { subset_2.push_back(objects[i]); }
   }
   else if(longest_axis == z)
   {
       if(objects[i]->centroid.z < splitting_point) { subset_1.push_back(objects[i]); }
       else { subset_2.push_back(objects[i]); }
   }
}
if(subset_1.empty() || subset_2.empty())
{
   if(!first_pass)
   {
       debugString("Unable to build AABB tree from all input objects. Maybe three or more objects have the same centroid?");
       return;
   }
   subset_1.clear(); subset_2.clear();
   splitting_point = ComputeObjectMean(longest_axis, objects);
   first_pass = false;
   goto create_subsets;
}
left = new AABB(subset_1); right = new AABB(subset_2);
    \end{lstlisting}
\end{figure}

U mojoj implementaciji je predvi�eno da listovi stabla (�vorovi koji nemaju djece) ne budu tipa \gls{aabb} ili neki ome�uju�i volumen, nego neki od \navod{konkretnih} objekata a to su sfere, povr�ine i trokuti. Zbog toga nema slu�ajeva kada ulazni skup objekata nema elemenata te tada nema ni konstrukcije objekta tipa \gls{aabb}. Ukoliko ome�uju�i volumen sadr�ava samo jedan objekt, onda je on spremljen u atribut \emph{left} dok \emph{right} ostaje NULL te je izgradnja stabla zavr�ena. Ukoliko ulazni skup ima dva objekta oni se spremaju u \emph{left} i \emph{right} te zavr�ava izgradnja stabla.

Kada ulazni skup objekata ima tri ili vi�e elemenata, prvi korak je izra�unati \gls{aabb} \emph{min} i \emph{max} atribute za njih. To se radi uz pomo� metode \emph{ComputeBoundingBox}, prikazanoj u isje�ku \ref{code:10.4}, koja je vrlo jednostavna. Ona pregledava minimalnu i maksimalnu to�ku svakog objekta u ulaznom skupu objekata \emph{objects} te odre�uje minimalne i maksimalne vrijednosti $ x $, $ y $ i $ z $ koordinata svih tih to�aka na temelju kojih se na kraju algoritma odre�uje \emph{min} i \emph{max} to�ka \gls{aabb}-a koji ome�uje sve te objekte.

\begin{figure}[!htbp]
    \begin{lstlisting}[style=myC++,label={code:10.4}, caption={Metoda ComputeBoundingBox}]
void ComputeBoundingBox(const SolidObjects& objects)
{
    Vector3f min = objects[0]->getMinPoint();
    Vector3f max = objects[0]->getMaxPoint();

    for(unsigned int i = 1; i < objects.size(); ++i)
    {
        if(objects[i]->getMinPoint().x < min.x)
            min.x = objects[i]->getMinPoint().x;

        if(objects[i]->getMinPoint().y < min.y)
            min.y = objects[i]->getMinPoint().y;

        if(objects[i]->getMinPoint().z < min.z)
            min.z = objects[i]->getMinPoint().z;

        if(objects[i]->getMaxPoint().x > max.x)
            max.x = objects[i]->getMaxPoint().x;

        if(objects[i]->getMaxPoint().y > max.y)
            max.y = objects[i]->getMaxPoint().y;

        if(objects[i]->getMaxPoint().z > max.z)
            max.z = objects[i]->getMaxPoint().z;
    }

    bounds[0] = min;
    bounds[1] = max;
}
    \end{lstlisting}
\end{figure}

Sljede�i korak je podjela ulaznog skupa tako da se \gls{aabb} podijeli ortogonalno na svojoj najduljoj strani. Ovaj pristup sam izabrao zato jer se mo�e obaviti u konstantnom vremenu, odnosno ne ovisi o objektima koji se nalaze unutar ome�uju�eg volumena. Tri su mogu�e strane, svaka se podudara sa jednom od tri osi: $ x $, $ y $ i $ z $. To�ka kojom se strana podijeli je to�ka podjele (eng. \emph{splitting point}). U ovoj implementaciji, to je sredina strane (eng. \emph{spatial median}) odnosno polovica strane. Izraz za izra�un to�ke podjele \eqref{eq:10.10} ukoliko je najdulja strana ona koja odgovara $ x $ osi  (izrazi za preostale dvije osi su analogni) odgovara zbroju $ x $ koordinate minimalne to�ke \gls{aabb}-a i polovici duljine najdulje osi.

\begin{equation}
splitting\,point = min_{x} + \frac{longest\,axis\,length}{2} \label{eq:10.10}
\end{equation}

Duljina najdulje osi se odre�uje tako da se prvo izra�una vektor \emph{MaxMin}, prema izrazu \eqref{eq:10.11} te se potra�i najve�a od troje izra�unatih koordinata ($x$, $y$ ili $z$).

\begin{equation}
MaxMin = max - min \label{eq:10.11}
\end{equation}

Uz ovakav pristup, mo�e se dogoditi da svi objekti zavr�e u samo jednom podskupu, s �ime sam se susreo kod izrade ovog rada. Algoritam u tom slu�aju pristupi drugoj metodi, koja ispravno funkcionira u ve�em broju slu�ajeva. Ta metoda pregleda centroide svih objekata u parametru \emph{objects}, prona�e minimalne i maksimalne vrijednosti za svaki od $x$, $y$ ili $z$ koordinata, spremi ih u \emph{min} i \emph{max} vektore te se na isti na�in, kao u izrazu \eqref{eq:10.11} izra�una vektor \emph{MaxMin} i potra�i najve�a od troje koordinata, kako bi se dobila najdulja os na kojoj �e se odrediti to�ka podjele. Sama to�ka podjele je prosje�na vrijednost koordinate centroida svih ulaznih objekata koja odgovara izabranoj najduljoj osi. Primjerice, ako je najdulja $ x $ os, algoritam uzima u obzir prosje�nu vrijednost $ x $ koordinata centroida svih ulaznih objekata.

Kada je to�ka podjele odre�ena, objekti se razvrstavaju ovisno o strani na kojoj se na�u. Sada se promatra samo ona koordinata koja odgovara odabranoj strani (osi). Primjerice, ako je odabrana strana $ x $, promatra se samo $ x $ koordinata centroida svih objekata u \emph{objects}. Ukoliko je ta koordinata manja od to�ke podjele, objekt ide u jedan podskup, ina�e ide u drugi.

Va�no je napomenuti kako se \gls{aabb} stablo ne mo�e ispravno izgraditi ukoliko korisnik programa u \emph{objects} postavi tri ili vi�e objekata s jednakim centroidima jer �e onda jedan podskup uvijek biti prazan.

Nakon uspje�nog odre�ivanja podskupa (slu�aj kada niti jedan od podskupa nije prazan), oni postaju ulazni skupovi u rekurzivnim pozivima istog konstruktora klase \gls{aabb}. Prvi podskup odlazi u lijevo podstablo a drugi u desno.

\section{U�inkovitost kori�tenja BVH-a}

U svrhu testiranja u�inkovitosti primjene \gls{bvh}-a, osmislio sam scenu, prikazanu u slici \ref{fig:bvh_test1}.

\begin{figure}[!htbp]
    \begin{center}
        \includegraphics[resolution=116]{BVHScene_WithoutAABB_272,442_lights_92}
        \caption{Scena za testiranje u�inkovitosti primjene BVH-a}
        \label{fig:bvh_test1}
    \end{center}
\end{figure}

Programski kod koji slu�i za generiranje scene u slu�aju kada se ne koristi \gls{bvh} nalazi se u isje�ku koda \ref{code:10.5} dok isje�ak \ref{code:10.6} prikazuje postavljanje iste scene uz pomo� objekta tipa \emph{\gls{aabb}} koji predstavlja \gls{aabb} stablo. 

\begin{figure}[!htbp]
    \begin{lstlisting}[style=myC++,label={code:10.5}, caption={Kod za generiranje scene bez kori�tenja BVH-a}]
void BVHScene_WithoutAABB(Scene& scene)
{
    scene.objects.push_back(new Sphere(Vector3f(-0.5f, -0.70f, -1.35f), 0.3f, Material(Color(0.0f, 1.0f, 1.0f), 0.5f)));
    scene.objects.push_back(new Sphere(Vector3f(0.5f, -0.70f, -1.65f), 0.3f, Material(Color(0.0f, 1.0f, 0.0f), 0.0f)));
    scene.objects.push_back(new Sphere(Vector3f(0.0f, -0.85f, -1.65f), 0.15f, Material(Color(0.0f, 1.0f, 0.5f), 0.2f)));
    scene.objects.push_back(new Sphere(Vector3f(-0.5f, -0.10f, -1.35f), 0.3f, Material(Color(1.0f, 1.0f, 0.2f), 0.1f)));
    scene.objects.push_back(new Sphere(Vector3f(-0.5f, 0.50f, -1.35f), 0.3f, Material(Color(1.0f, 192.0f/255.0f, 203.0f/255.0f), 0.7f)));
    
    ... // kod za instanciranje "kutije"
    
    scene.addAreaLightUniform(0.08f);
}

    \end{lstlisting}
\end{figure}

\begin{figure}[!htbp]
    \begin{lstlisting}[style=myC++,label={code:10.6}, caption={Kod za generiranje scene uz pomo� BVH-a}]
void BVHScene_WithAABB(Scene& scene)
{
    SolidObjects objs;

    objs.push_back(new Sphere(Vector3f(-0.5f, -0.70f, -1.35f), 0.3f, Material(Color(0.0f, 1.0f, 1.0f), 0.5f)));
    objs.push_back(new Sphere(Vector3f(0.5f, -0.70f, -1.65f), 0.3f, Material(Color(0.0f, 1.0f, 0.0f), 0.0f)));
    objs.push_back(new Sphere(Vector3f(0.0f, -0.85f, -1.65f), 0.15f, Material(Color(0.0f, 1.0f, 0.5f), 0.2f)));
    objs.push_back(new Sphere(Vector3f(-0.5f, -0.10f, -1.35f), 0.3f, Material(Color(1.0f, 1.0f, 0.2f), 0.1f)));
    objs.push_back(new Sphere(Vector3f(-0.5f, 0.50f, -1.35f), 0.3f, Material(Color(1.0f, 192.0f/255.0f, 203.0f/255.0f), 0.7f)));
    
    ... // kod za instanciranje "kutije"

    scene.objects.push_back(new AABB(objs));

    scene.addAreaLightUniform(0.08f);
}
    \end{lstlisting}
\end{figure}

Ostali va�ni parametri su:

\begin{itemize}
\item broj svjetala: 92
\item antialiasing: 2
\end{itemize}

Izmjerena vremena generiranja slike:

\begin{itemize}
\item bez \gls{bvh}-a: 272,442 s
\item uz pomo� \gls{bvh}-a: 247,362 s
\end{itemize}

Zaklju�ujem kako je moja implementacija skratila vrijeme potrebno za generiranje scene i time ubrzala performanse cjelokupnog programa.